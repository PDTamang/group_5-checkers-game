package CheckerGame;

import java.util.Scanner;

import CheckerGame.Easy.EasyMode;
import CheckerGame.Medium.MediumMode;
import CheckerGame.Hard.HardMode;

public class MainGame {
        public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        PiecesAndMesg ps = new PiecesAndMesg();
        TerminalFonts ts = new TerminalFonts();
        EasyMode easyMode = new EasyMode();
        MediumMode mediumMode = new MediumMode();
        HardMode hardMode = new HardMode();
        LogicSupporter ls = new LogicSupporter();

        ps.hello();
        ps.rules();

        boolean play = true;
        while (play) {
            ps.gameModeSelection();
            System.out.println("\n"+ps.padding+ts.ANSI_YELLOW + "Now Selecte the gameMode" + ts.ANSI_RESET+"\n");

            int gameMode = scan.nextInt();
            while(gameMode != 1 && gameMode != 2 && gameMode != 3){
                ps.gameModeSelection();
                System.out.println(ps.padding+ts.ANSI_YELLOW + "Reselect the gameMode" + ts.ANSI_RESET+"\n");
                gameMode = scan.nextInt();
            }
            if(gameMode == 1){
                System.out.println(ps.padding+ts.ANSI_GREEN + "Easy-Mode Selected" + ts.ANSI_RESET+"\n");
                easyMode.main();
                play = ls.playAgian();
                
            }else if(gameMode == 2){
                System.out.println(ps.padding+ts.ANSI_GREEN + "Medium-Mode Selected" + ts.ANSI_RESET+"\n");
                mediumMode.main();
                
                play = ls.playAgian();

            }else{  
                System.out.println(ps.padding+ts.ANSI_GREEN + "Hard-Mode Selected" + ts.ANSI_RESET+"\n");
                hardMode.main();
                play = ls.playAgian();
            }

        }
        ps.thankYou();
        scan.close();
    }
}
