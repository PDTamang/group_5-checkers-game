package CheckerGame;

public class PiecesAndMesg {
    TerminalFonts ts = new TerminalFonts();
    public String wall = "xx";
    public String empty = "__";
    public String value = "";

    public String player1 = "P1";
    public String player2  = "P2";
    public String padding = "                                          ";
    public String player1K = "1k";
    public String player2K = "2k";
    public int totalMove = 0;

    public String playAgainMsg = padding+ts.ANSI_YELLOW + "PlayAgain [y/n]: " + ts.ANSI_RESET;

    public String giveChanceMsg = padding+ts.ANSI_YELLOW + "giveChance [y/n]: " + ts.ANSI_RESET;
    public String chanceGivenTO = padding+ts.ANSI_YELLOW + "You have given a chance " + ts.ANSI_RESET;

    public String ply2 = padding+ts.ANSI_YELLOW + "NOW: Turn: Player_2" + ts.ANSI_RESET;
    public String ply1 = padding+ts.ANSI_YELLOW + "NOW: Turn: Player_1" + ts.ANSI_RESET;

    public String p1Win = ts.ANSI_GREEN +" WON THE GAME ***"+ ts.ANSI_RESET;
    public String p2Win = ts.ANSI_GREEN +" WON THE GAME ***"+ ts.ANSI_RESET;

    public void errorMsg(){
        System.out.println("\n"+padding+ts.ANSI_RED + "ERROR: This move is 'NOT-VALID'" + ts.ANSI_RESET+"\n");
    }

    public void rules(){
        System.out.println("\n"+padding+"           "+ts.ANSI_YELLOW + "Welcom in our CHECKER GAME" + ts.ANSI_RESET+"\n");
        System.out.println(padding+ts.ANSI_GREEN + "Here is some Rules that you shuld know 👍👍👍" + ts.ANSI_RESET);
        System.out.println(padding+ts.ANSI_GREEN + "1)Setup: " + ts.ANSI_RESET);
        System.out.println(padding+"  "+ts.ANSI_GREEN + "The board is set up with each player's pieces on the dark squares of their side of the board.\r\n" + padding+"  "+
                                                            "Players place their pieces on the three rows closest to them. " + ts.ANSI_RESET);
        System.out.println(padding+ts.ANSI_GREEN + "2)Movement: " + ts.ANSI_RESET);
        System.out.println(padding+"  "+ts.ANSI_GREEN + "Regular pieces (p1 or p2) move forward diagonally one squre at a time.\r\n" + padding+"  "+
                                                            "They capture opponent pieces by jumping over them diagonally to an empty square immediately beyond.\r\n" + padding+"  "+
                                                            "Capturing is mandatory when possible, and if a player has multiple capturing options, they must choose\r\n" + padding+"  "+
                                                            "the option that captures the most pieces. Men are promoted to kings when they reach the last row on\r\n" + padding+"  "+
                                                            "the opponent's side of the board. " + ts.ANSI_RESET);
        System.out.println(padding+ts.ANSI_GREEN + "3)Kinged Pieces (k2 or k1): "+ ts.ANSI_RESET);
        System.out.println(padding+"  "+ts.ANSI_GREEN + "When a regular piece reaches the last row on the opponent's side, it is promoted to a king.\r\n" + padding+"  "+
                                                            "Kings can move and capture diagonally both forward and backward."+ts.ANSI_RESET);       
        System.out.println(padding+ts.ANSI_GREEN + "3)End of Game:"+ ts.ANSI_RESET);
        System.out.println(padding+"  "+ts.ANSI_GREEN + "he game ends when one player captures all of the opponent's pieces"+ts.ANSI_RESET);  
    }

    public void hello(){
        String[] hello = {
            "#   #  #####  #      #      ### ",
            "#   #  #      #      #     #   #",
            "#####  ####   #      #     #   #",
            "#   #  #      #      #     #   #",
            "#   #  #####  #####  #####  ### "
        };

        for (String line : hello) {
            System.out.println("\n"+padding+"           "+ts.ANSI_BLUE+line+ ts.ANSI_RESET);
        }
    }

    public void thankYou() {
        String[] thankYou = {
            "#######   ### ",
            "   #     #   #",
            "   #    #     #",
            "   #     #  # #",
            "   #      ### #"
        };
    
        for (String line : thankYou) {
            System.out.println("\n" + padding + "           " + ts.ANSI_GREEN + line + ts.ANSI_RESET);
        }
        System.out.println("\n" + padding + "         " + ts.ANSI_YELLOW + "For Playing this game" + ts.ANSI_RESET+"\n");
    }
    


    public void gameModeSelection(){
        System.out.println("\n"+padding+ts.ANSI_YELLOW + "The Game Mode Available are: " + ts.ANSI_RESET+"\n");
        System.out.println(padding+"           "+ts.ANSI_YELLOW + "click 1 -----> Easy Mode" + ts.ANSI_RESET);
        System.out.println(padding+"           "+ts.ANSI_YELLOW + "click 2 -----> Medium Mode" + ts.ANSI_RESET);
        System.out.println(padding+"           "+ts.ANSI_YELLOW + "click 3 -----> Hard Mode" + ts.ANSI_RESET+"\n");
    }


}
