package CheckerGame;

public class  HashTableDC{ 
    LinkList[] node;
    int size = 8;

    public HashTableDC(){
        node = new LinkList[size];
    }

    void resize(){
        size *= 2;
        LinkList[] newArr = new LinkList[size];
        for(int i=0; i<size; i++){
            newArr[i] = node[i];
        }
        node = newArr;
    }
    public void insert(String intg){
        int mod = modASCII(intg, size);
        if (mod>=size){
            resize();
        }
        if(node[mod] == null) {
            node[mod] = new LinkList();
        }
        node[mod].insertion(intg);
    }

    void nodeEleSize(){
        for (int i=0; i<size; i++){
            if (node[i] != null){
                System.out.print(node[i].size+" ");
            }
        }
        System.out.println();
    }
    void traverse(){
        for (int i=0; i<size; i++){
            if(node[i] != null) {
                node[i].traverse();
            }
        }
    }

    public String search(String value){
        for (int i=0; i<size; i++){
            if (node[i] !=null){
                if(node[i].searching(value)){
                    System.out.println("'"+value+"' is in hashing table");
                    return value;
                }
            }
        }
        return value;

    }


    public int modASCII(String word, int cellNumber) {
        int total = 0;
        for (int i=0; i<word.length(); i++) {
            total += word.charAt(i);
        }
        return total % cellNumber;
    }
    void delete(String value){
        for (int i=0; i<size; i++){
            if (node[i] !=null){
                if(node[i].delete(value)){
                    System.out.println("'"+value+"' has been deleted");
                    return;
                }
            }
        }
        System.out.println("'"+value+"' could not delete. Not found");
    }

    public static void main(String[] args) {
        HashTableDC ht = new HashTableDC();
        ht.insert("ABCD");
        ht.insert("EFGH");
        ht.insert("IJKL");
        ht.insert("IJKLM");
        ht.insert("Sangay");

        System.out.println("size");
        ht.nodeEleSize();

        System.out.println("===============================================================\n");
        System.out.println("TRAVERSE");
        ht.traverse();

        System.out.println("===============================================================\n");
        System.out.println("SEARCHING");
        ht.search("ABCDldk");


        System.out.println("===============================================================\n");
        System.out.println("DELETION");
        ht.delete("ABCD");
    }
}