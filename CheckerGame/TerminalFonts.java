package CheckerGame;
public class TerminalFonts {
    // ANSI escape codes for text formatting
    public String ANSI_RESET = "\u001B[0m";
    public  String ANSI_BOLD = "\u001B[1m";
    public  String ANSI_ITALIC = "\u001B[3m";
    public  String ANSI_UNDERLINE = "\u001B[4m";
    public  String ANSI_RED = "\u001B[31m";
    public  String ANSI_GREEN = "\u001B[32m";
    public  String ANSI_YELLOW = "\u001B[33m";
    public  String ANSI_BLUE = "\u001B[34m";

    public static void main(String[] args) {
        // Example usage
        // System.out.println(ANSI_BOLD + "This is bold text" + ANSI_RESET);
        // System.out.println(ANSI_ITALIC + "This is italic text" + ANSI_RESET);
        // System.out.println(ANSI_UNDERLINE + "This is underlined text" + ANSI_RESET);

        // // Colored text
        // System.out.println(ANSI_RED + "This is red text" + ANSI_RESET);
        // System.out.println(ANSI_GREEN + "This is green text" + ANSI_RESET);
        // System.out.println(ANSI_YELLOW + "This is yellow text" + ANSI_RESET);
        // System.out.println(ANSI_BLUE + "This is blue text" + ANSI_RESET);
    }
}

