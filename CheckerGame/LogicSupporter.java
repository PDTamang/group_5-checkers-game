package CheckerGame;
// import CheckerGame.TerminalFonts;
import java.util.Scanner;


   
public class LogicSupporter {
     PiecesAndMesg ps = new PiecesAndMesg();
     Scanner scan = new Scanner(System.in);


    public boolean givenChance(){
        System.out.print(ps.giveChanceMsg);
        String play = scan.nextLine();
        boolean check = true;
        boolean giveChance = false;
        while (check) {
            if(play.equals("y")){
                System.out.println("YES");
                giveChance =  true;
                break;
            }else if (play.equals("n")){
                System.out.println("NO");
                giveChance = false;
                break;
            }
            System.out.print(ps.giveChanceMsg);
            play = scan.nextLine();
        }
        return giveChance;
    }

    public void printErr(){
        ps.errorMsg();
        System.out.println();  
    }

    public int searchKing(String[][] gameBoard){
        for(int i=0; i<gameBoard.length; i++){
            for(int j=0; j<gameBoard.length; j++){
                if(gameBoard[i][j] == ps.player1K){
                    return 1;
                }else if(gameBoard[i][j] == ps.player2K){
                    return 2;
                }
            }
        }
        return 0;
    }

    public boolean playAgian(){
        System.out.print(ps.playAgainMsg);
        String ok = scan.nextLine();
        boolean check = true;
        boolean playag = false;
        while (check) {
            if(ok.equals("y")){
                System.out.println("YES");
                playag =  true;
                break;
            }else if (ok.equals("n")){
                System.out.println("NO");
                playag = false;
                break;
            }
            System.out.print(ps.playAgainMsg);
            ok = scan.nextLine();
        }
        return playag;
    }
}
