package CheckerGame.Easy;

import CheckerGame.LostPsStack;
import CheckerGame.PiecesAndMesg;
import CheckerGame.TerminalFonts;
import CheckerGame.LogicSupporter;



public class GameLogicDown {
    PiecesAndMesg ps = new PiecesAndMesg();
    LostPsStack lpq = new LostPsStack();
    TerminalFonts ts = new TerminalFonts();
    LogicSupporter ls = new LogicSupporter();

    public void traverse(String[][] gameBoard){
        System.out.println(ps.padding+"Total move: "+ps.totalMove++);
        for(int i=0; i<gameBoard.length; i++){
            for(int j=0; j<gameBoard.length; j++){
                System.out.print(gameBoard[i][j]+ " ");
            }
            System.out.println();
        }
        System.out.println();
    }

  public boolean moveDownLeft(String[][] gameBoard,int l, int w, String Cp){
        boolean error = false;
        if (w-1 == 0){
           ls.printErr();
            error = true;               
        }else if(l+1 >= 1){
            if (Cp == ps.player1 || Cp == ps.player1K){
                System.out.println("p1");
                if (l+2 >=1 && gameBoard[l+1][w-1] == ps.player2 || gameBoard[l+1][w-1] == ps.player2K && gameBoard[l+2][w-2] == ps.empty){
                        lpq.push(ts.ANSI_RED +gameBoard[l+1][w-1]+ ts.ANSI_RESET);
                        gameBoard[l+1][w-1] = ps.empty;
                        gameBoard[l][w] = ps.empty;
                    if (l+2 == gameBoard.length-1){
                        System.out.println("jgood");
                        if (Cp != ps.player1K && Cp != ps.player2K){
                            gameBoard[l+2][w-2] = ps.player1K;
                        }else {
                            gameBoard[l+2][w-2] = Cp;
                        }
                    }else {
                        gameBoard[l+2][w-2] = Cp;
                    }
                    traverse(gameBoard);
                    if (!lpq.isEmpty()){
                        System.out.println("\n"+ts.ANSI_GREEN +"LostPieces: "+ ts.ANSI_RESET);
                        lpq.traverse();
                    }
                }else if (gameBoard[l+1][w-1] == ps.empty){
                    // System.out.print("empty here");
                    gameBoard[l][w] = ps.empty;
                    if (l+1 == gameBoard.length-1){
                        if (Cp != ps.player1K && Cp != ps.player2K){
                            gameBoard[l+1][w-1] = ps.player1K; 
                        }else {
                            gameBoard[l+1][w-1] = Cp; 
                        }
                    }else {
                        gameBoard[l+1][w-1] = Cp; 
                    }
                    traverse(gameBoard); 
                    if (!lpq.isEmpty()){
                        System.out.println("\n"+ts.ANSI_GREEN +"LostPieces: "+ ts.ANSI_RESET);
                        lpq.traverse();
                    }
                }else {
                    ls.printErr();
                    error = true;
                }
            }else if (Cp == ps.player2K){
               if (l+2 >=1 && gameBoard[l+1][w-1] == ps.player1 || gameBoard[l+1][w-1] == ps.player1K && gameBoard[l+2][w-2] == ps.empty){
                        lpq.push(ts.ANSI_RED +gameBoard[l+1][w-1]+ ts.ANSI_RESET);
                        gameBoard[l+1][w-1] = ps.empty;
                        gameBoard[l][w] = ps.empty;
                    if (l+2 == gameBoard.length-1){
                        if (Cp != ps.player1K && Cp != ps.player2K){
                            gameBoard[l+2][w-2] = ps.player1K;
                        }else {
                            gameBoard[l+2][w-2] = Cp;
                        }
                    }else {
                        gameBoard[l+2][w-2] = Cp;
                    }
                    traverse(gameBoard);
                    if (!lpq.isEmpty()){
                        System.out.println("\n"+ts.ANSI_GREEN +"LostPieces: "+ ts.ANSI_RESET);
                        lpq.traverse();
                    }
                }else if (gameBoard[l+1][w-1] == ps.empty){
                    gameBoard[l][w] = ps.empty;
                    if (l+1 == gameBoard.length-1){
                        if (Cp != ps.player1K && Cp != ps.player2K){
                            gameBoard[l+1][w-1] = ps.player1K; 
                        }else {
                            gameBoard[l+1][w-1] = Cp; 
                        }
                    }else {
                        gameBoard[l+1][w-1] = Cp; 

                    }
                    traverse(gameBoard); 
                    if (!lpq.isEmpty()){
                        System.out.println("\n"+ts.ANSI_GREEN +"LostPieces: "+ ts.ANSI_RESET);
                        lpq.traverse();
                    }
                }else {
                   ls.printErr();
                error = true;
                   
                }
            }

        }else{
                ls.printErr();
                error = true;
        }       
       
        return error;      
    }

    public boolean moveDownRight(String[][] gameBoard,int l, int w, String Cp){
            boolean error = false;
            if (w+1 >= gameBoard.length){
               ls.printErr();
                error = true;      
            }else if(l+1 >= 1){
                if (Cp == ps.player1 || Cp == ps.player1K){
                    if (l+2 >=1 && gameBoard[l+1][w+1] == ps.player2 || gameBoard[l+1][w+1] == ps.player2K && gameBoard[l+2][w+2] == ps.empty){
                            lpq.push(ts.ANSI_RED +gameBoard[l+1][w+1]+ ts.ANSI_RESET);
                            gameBoard[l+1][w+1] = ps.empty;
                            gameBoard[l][w] = ps.empty;
                        if (l+2 == gameBoard.length-1){
                            if (Cp != ps.player1K && Cp != ps.player2K){
                                gameBoard[l+2][w+2] = ps.player1K;
                            }else {
                                gameBoard[l+2][w+2] = Cp;
                            }
                        }else {
                            gameBoard[l+2][w+2] = Cp;
                        }
                        traverse(gameBoard);
                        if (!lpq.isEmpty()){
                            System.out.println("\n"+ts.ANSI_GREEN +"LostPieces: "+ ts.ANSI_RESET);
                            lpq.traverse();
                        }
                    }else if (gameBoard[l+1][w+1] == ps.empty){
                        gameBoard[l][w] = ps.empty;
                        if (l+1 == gameBoard.length-1){
                            if (Cp != ps.player1K && Cp != ps.player2K){
                                gameBoard[l+1][w+1] = ps.player1K;
                            }else {
                                gameBoard[l+1][w+1] = Cp; 
                            }  
                        }else {
                            gameBoard[l+1][w+1] = Cp; 

                        }
                        traverse(gameBoard); 
                        if (!lpq.isEmpty()){
                            System.out.println("\n"+ts.ANSI_GREEN +"LostPieces: "+ ts.ANSI_RESET);
                            lpq.traverse();
                        }
                    }else {
                        ls.printErr();  
                        error = true;
                    }
                }else if (Cp == ps.player2K){
                    System.out.println("heee");
                    if (l+2 >=1 && gameBoard[l+1][w+1] == ps.player1 || gameBoard[l+1][w-1] == ps.player1K && gameBoard[l+2][w+2] == ps.empty){
                            System.out.println("ok");
                            lpq.push(ts.ANSI_RED +gameBoard[l+1][w+1]+ ts.ANSI_RESET);
                            gameBoard[l+1][w+1] = ps.empty;
                            gameBoard[l][w] = ps.empty;
                        if (l+2 == gameBoard.length-1){
                            if (Cp != ps.player1K && Cp != ps.player2K){
                                gameBoard[l+2][w+2] = ps.player1K;
                            }else {
                                gameBoard[l+2][w+2] = Cp;
                            }
                        }else {
                            gameBoard[l+2][w+2] = Cp;
                        }
                        traverse(gameBoard);
                        if (!lpq.isEmpty()){
                            System.out.println("\n"+ts.ANSI_GREEN +"LostPieces: "+ ts.ANSI_RESET);
                            lpq.traverse();
                        }
                    }else if (gameBoard[l+1][w+1] == ps.empty){
                        gameBoard[l][w] = ps.empty;
                        if (l+1 == gameBoard.length-1){
                            if (Cp != ps.player1K && Cp != ps.player2K){
                                gameBoard[l+1][w+1] = ps.player2K; 
                            }else {
                                gameBoard[l+1][w+1] = Cp; 
                            }
                        }else {
                            gameBoard[l+1][w+1] = Cp; 

                        }
                        traverse(gameBoard); 
                        if (!lpq.isEmpty()){
                            System.out.println("\n"+ts.ANSI_GREEN +"LostPieces: "+ ts.ANSI_RESET);
                            lpq.traverse();
                        }
                    }else {
                        ls.printErr();
                        error = true; 
                    
                    }
                }
            }else{
                    ls.printErr();
                    error = true;
            }       
        
        return error;      
    }
}

