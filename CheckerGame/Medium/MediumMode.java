package CheckerGame.Medium;

import java.util.Scanner;
import java.util.Random;
import CheckerGame.PiecesAndMesg;
import CheckerGame.TerminalFonts;
import CheckerGame.GameBoard;
import CheckerGame.HashTableDC;

// import MainGame;
public class MediumMode {
    GameController gc = new GameController();
    Scanner scan = new Scanner(System.in);
    Random random = new Random();
    GameBoard gb = new GameBoard();
    PiecesAndMesg ps = new PiecesAndMesg();
    TerminalFonts ts = new TerminalFonts();


    HashTableDC players = new HashTableDC();

    void setPlayer(String p1, String p2) {
        players.insert(p1);
        players.insert(p2);
    }

    public void main() {
        GameController gc = new GameController();
        Scanner scan = new Scanner(System.in);
        GameBoard gb = new GameBoard();
        PiecesAndMesg ps = new PiecesAndMesg();
        TerminalFonts ts = new TerminalFonts();
        MediumMode gm = new MediumMode();
      
        System.out.println(ps.padding+"OK! Before stating the game can you give me you player name please");
        System.out.println();
        System.out.print(ps.padding+ts.ANSI_YELLOW+"Player_1's name: "+ts.ANSI_RESET);
        String p1 = scan.nextLine();

        System.out.print(ps.padding+ts.ANSI_YELLOW+"Player_2's name: "+ts.ANSI_RESET);
        String p2 = scan.nextLine();
        gm.setPlayer(p1, p2);

        System.out.print(ps.padding+ts.ANSI_YELLOW+"Can you put the size of the board that you wanna play?: "+ts.ANSI_RESET);
        int size = scan.nextInt();
        boolean err = gb.boardSize(size+1);
        if(err){
            gb.board();
            boolean startGame = true;
            System.out.println();
            int player = random.nextInt(2) + 1;

            while(startGame) {
                int checkWin = gc.win(gb.gameBoard);
                if (checkWin == 1){
                    System.out.println(ps.padding+"*** "+players.search(p1)+ps.p1Win);
                    startGame = false;
                }else if (checkWin == 2){
                    System.out.println(ps.padding+"*** "+players.search(p2)+ps.p2Win);
                    startGame = false;
                } else {
                    if (player == 1) {
                        player = 2;
                        System.out.println(ps.ply2+" "+players.search(p2));
                    }else if (player == 2){
                        player = 1;                  
                        System.out.println(ps.ply1+" "+players.search(p1));
                    }
                    System.out.println(ps.padding+ts.ANSI_RED+"Heart: +"+ ts.ANSI_RESET);
                    System.out.println(ps.padding+"Enter the 'ROW-NUMBER'");
                    int length = scan.nextInt();
                    System.out.println(ps.padding+"Enter the 'COLUMN-NUMBER'");
                    int width = scan.nextInt();
                    scan.nextLine();
                    gc.move(gb.gameBoard,length, width, player);
                }
            }

        }
       

    }
}

