package CheckerGame;

class Node {
    String data;
    Node next;

    Node(String data) {
        this.data = data;
        this.next = null;
    }
}

public class LinkList {
    Node head;
    Node tail;
    int size;

    LinkList() {
        this.head = null;
    }

    void traverse() {
        Node current = head;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();
    }

    void insertion (String element){
        Node newNode = new Node(element);
        if(head == null){
            head = newNode;
            tail = newNode;
        } else{
             tail.next = newNode;
            tail = newNode; 
        }   
        size++;
    }

    boolean searching(String ele){
        Node current = head;

        boolean check = false;
        while(check == false){
            if (current.data == ele){
                check = true;
            }
            else if(current.next == null){
                return check;
            }
            current = current.next;
        }
        return check;
    }

    boolean delete (String el){
        if(searching(el)){
            Node current = head;
            int i=0;
            while (current.data != el) {
            current = current.next;
                i++;
            }
            Node anotherC = head;
            int j = 0;
            while (j<i-1){
                anotherC = anotherC.next;
                j++;
            }        
            anotherC.next= current.next ;
            current.next = anotherC;
            return true;
        }else{
            return false;
        }

    }

    String accessing(int index){
        Node current = head;
        int i=0;
        while(i<index){
            current = current.next;
            i++;
        }

        return current.data;
    }

}