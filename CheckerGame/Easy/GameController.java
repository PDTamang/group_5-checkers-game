package CheckerGame.Easy;
import java.util.Scanner;

import CheckerGame.PiecesAndMesg;

public class GameController {
    Scanner scan = new Scanner(System.in);
    PiecesAndMesg ps = new PiecesAndMesg();
    // GameBoard gb = new GameBoard();
    GameLogicUp glu = new GameLogicUp();
     GameLogicDown gld = new GameLogicDown();



    public boolean move(String[][] gameBoard, int l, int w,  int player){
        boolean error = false;
        if (l>=gameBoard.length || l<=0){
            System.out.println(2);
            ps.errorMsg();
            System.out.println(); 
            error = true;        
        }else {
            String Cp = gameBoard[l][w];
            System.out.println(Cp+"  "+player);
            if (Cp == ps.player1 && player ==1|| Cp == ps.player2 && player ==2){
                System.out.println(ps.padding+"Enter 'l' to move leftfront OR Enter 'r' to move rightfront");
                String shift = scan.nextLine();
                if (player == 2) {
                    System.out.println(shift);
                    if (shift.equals("l")){
                        error = glu.moveUpLeft(gameBoard,l,w,Cp);
                    }else if (shift.equals("r")) {
                        error = glu.moveUpRight(gameBoard,l,w,Cp);
                    }
                }else if (player == 1) {
                    if (shift.equals("l")){
                        error = gld.moveDownLeft(gameBoard,l,w,Cp);
                    }else if (shift.equals("r")) {
                        error = gld.moveDownRight(gameBoard,l,w,Cp);
                    }
                }
            }else if (Cp == ps.player1K && player ==1 || Cp == ps.player2K && player ==2){
                System.out.println(ps.padding+"Enter 'ul' to move leftUp AND Enter 'ur' OR to move rightUp 'dl' to move leftDown AND 'dr' to move rightDown" );
                String shift = scan.nextLine();
                    System.out.println(shift);
                    if (shift.equals("ul")){
                       error = glu.moveUpLeft(gameBoard,l,w,Cp);
                    }else if (shift.equals("ur")) {
                        error = glu.moveUpRight(gameBoard,l,w,Cp);
                    }else if (shift.equals("dl")){
                        error = gld.moveDownLeft(gameBoard,l,w,Cp);
                    }else if (shift.equals("dr")) {
                        error = gld.moveDownRight(gameBoard,l,w,Cp);
                    }
            }else{
                ps.errorMsg();
                System.out.println(); 
                error = true;   
            }
        }
        return error;
    }

    public int win(String[][] gameBoard){
        boolean checkP1 = false;
        boolean checkP2 = false;
        int winner = 0;
        for(int i=0; i<gameBoard.length; i++){
            for(int j=0; j<gameBoard.length; j++){
                if (gameBoard[i][j] == ps.player1 || gameBoard[i][j] == ps.player1K){
                    checkP1 = true;
                };

                if (gameBoard[i][j] == ps.player2 || gameBoard[i][j] == ps.player2K) {
                    checkP2 = true;
                }
            }
        }

        if (checkP1 == true && checkP2 == false) {
            winner = 1;
        } else if (checkP1 == false && checkP2 == true){
            winner =  2;
        } else {
            winner = 0;
        }
        return winner;
    }
}
