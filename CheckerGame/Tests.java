package CheckerGame;
import CheckerGame.Easy.GameController;
import CheckerGame.Easy.GameLogicDown;
import CheckerGame.Easy.GameLogicUp;


public class Tests {
    GameBoard gb = new GameBoard();
    GameLogicUp glu = new GameLogicUp();
    GameLogicDown gld = new GameLogicDown();
    PiecesAndMesg ps = new PiecesAndMesg();



    GameController gc = new GameController();
    boolean wrongMove(){
        int l = 3;
        int w = 3;
        String Cp = "P2";
        boolean board = gb.boardSize(4);
        gb.board();
        
        boolean err = glu.moveUpRight(gb.gameBoard,l,w,Cp);;
        if (err == true){
            return true;
        }else {
            return false;
        }
        
    }
    boolean wrongMan(){
        int l = 3;
        int w = 1;
        String Cp = "P2";
        boolean board = gb.boardSize(7);
        gb.board();

        
        boolean err = glu.moveUpLeft(gb.gameBoard,l,w,Cp);;
        if (err == true){
            return true;
        }else {
            return false;
        }
    }

    boolean jumpingOverYourSelf(){
        int l = 6;
        int w = 6;
        String Cp = "P2";
        boolean board = gb.boardSize(7);
        gb.board();

        
        boolean err = glu.moveUpRight(gb.gameBoard,l,w,Cp);
        if (err == true){
            return true;
        }else {
            return false;
        }
    }
    boolean knockoutOppents(){
        int l = 5;
        int w = 5;
        String Cp = "P2";
        boolean board = gb.boardSize(7);
        gb.board();
        boolean er = glu.moveUpLeft(gb.gameBoard,l,w,Cp);;
        if(er){
            return er;
        }
        l = 2;
        w = 2;
        Cp = "P1";
    
        boolean err = gld.moveDownRight(gb.gameBoard,l,w,Cp);
         if(err){
            return er;
        }

        l = 4;
        w = 4;
        Cp = "P2";
        boolean errr = glu.moveUpLeft(gb.gameBoard,l,w,Cp);;
        if(!errr){
            return true;
        }else {
            return false;
        }
    }
    boolean win(){
        int l = 3;
        int w = 3;
        String Cp = "P2";
        int winNo;
        boolean board = gb.boardSize(4);
        gb.board();

        boolean er = glu.moveUpLeft(gb.gameBoard,l,w,Cp);;
        winNo = gc.win(gb.gameBoard);
        if (winNo == 1){
            return true;
        }

        l = 1;
        w = 1;
        Cp = "P1";

        boolean err = gld.moveDownRight(gb.gameBoard,l,w,Cp);
        winNo = gc.win(gb.gameBoard);
        if (winNo == 1){
            return true;
        }
        l = 3;
        w = 1;
        Cp = "P2";

        boolean errr = glu.moveUpRight(gb.gameBoard,l,w,Cp);
        winNo = gc.win(gb.gameBoard);
        if (winNo == 1){
            return true;
        }

        l = 3;
        w = 3;
        Cp = "1k";

        boolean errrr = glu.moveUpLeft(gb.gameBoard,l,w,Cp);;
        winNo = gc.win(gb.gameBoard);
        if (winNo == 1 || CheckWinner(gb.gameBoard) == 1){
            return true;
        }else{
            return false;
        }
    }


    int CheckWinner(String[][] gameBoard){
        boolean checkP1 = false;
        boolean checkP2 = false;
        int winner = 0;
        for(int i=0; i<gameBoard.length; i++){
            for(int j=0; j<gameBoard.length; j++){
                if (gameBoard[i][j] == ps.player1 || gameBoard[i][j] == ps.player1K){
                    checkP1 = true;
                };

                if (gameBoard[i][j] == ps.player2 || gameBoard[i][j] == ps.player2K) {
                    checkP2 = true;
                }
            }
        }

        if (checkP1 == true && checkP2 == false) {
            winner = 1;
        } else if (checkP1 == false && checkP2 == true){
            winner =  2;
        } else {
            winner = 0;
        }
        return winner;
    }

    boolean selectingOtherThanPieces(){
        int l = 2;
        int w = 1;
        boolean board = gb.boardSize(4);
        gb.board();
        boolean err = gc.move(gb.gameBoard, l, w, 1);
        if(err){
            return true; 
        }else {
            return false;
        }
    }

    public static void main(String[] args) {
        PiecesAndMesg ps = new PiecesAndMesg();
        Tests tst = new Tests();
        boolean t1 = tst.wrongMove();
        boolean t2 = tst.wrongMan();
        boolean t3 = tst.jumpingOverYourSelf();
        boolean t4 = tst.knockoutOppents();
        boolean t5 = tst.win();
        boolean t6 = tst.selectingOtherThanPieces();


        if(t1){
            System.out.print(ps.padding+"Test 1 ----- wrong move ------>  ");
            System.out.println("Pass\n");
        }else {
            System.out.print(ps.padding+"Test 1 ----- wrong move ------>  ");
            System.out.println("Fail\n");
        }
        if(t2){
            System.out.print(ps.padding+"Test 2 ------ wrong Man ------>  ");
            System.out.println("Pass\n");
        }else {
            System.out.print(ps.padding+"Test 2 ------ wrong Man ------>  ");
            System.out.println("Fail\n");
        }
        if(t3){
            System.out.print(ps.padding+"Test 3 --- knoking urself ---->  ");
            System.out.println("Pass\n");
        }else {
            System.out.print(ps.padding+"Test 3 --- knoking urself ---->  ");
            System.out.println("Fail\n");
        }
        if(t4){
            System.out.print(ps.padding+"Test 4 ---- knocking enemy --->  ");
            System.out.println("Pass\n");
        }else {
            System.out.print(ps.padding+"Test 4 ---- knocking enemy --->  ");
            System.out.println("Fail\n");
        } if(t5){
            System.out.print(ps.padding+"Test 5 --------- win --------->  ");
            System.out.println("Pass\n");
        }else {
            System.out.print(ps.padding+"Test 5 --------- win --------->  ");
            System.out.println("Fail\n");
        }if(t6){
            System.out.print(ps.padding+"Test 6 -----selectingOtherThanPieces----->  ");
            System.out.println("Pass\n");
        }else {
            System.out.print(ps.padding+"Test 6 -----selectingOtherThanPieces----->  ");
            System.out.println("Fail\n");
        }
    }
}
