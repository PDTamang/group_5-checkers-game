package CheckerGame;

public class GameBoard {
    public PiecesAndMesg ps = new PiecesAndMesg();
    public String gameBoard[][];
    public int size;

    public boolean boardSize(int size){
        if (size<4){
            System.out.println();
            System.out.println(ps.padding+"Size of the GameBord cannot be smaller than 4X4");
            System.out.println();
            return false;
        }else{
            this.size = size;
            gameBoard = new String[size][size];
            return true;
        }
       
    }


    private void defaultBoard(int l){
        if (ps.value == ps.empty || ps.value == ps.player1 || ps.value == ps.player2){
            ps.value = ps.wall;
        }else {
            if(size <7){
                if(l <= 1) ps.value = ps.player1;
                else if (l>=size-1) ps.value = ps.player2;
                else ps.value = ps.empty;
            }else if(size<9 && size>=7 ){
                if(l <= 2) ps.value = ps.player1;
                else if (l>=5) ps.value = ps.player2;
                else ps.value = ps.empty;
            }else if (size>=9){
                if(l <= 3) ps.value = ps.player1;
                else if (l>=6) ps.value = ps.player2;
                else ps.value = ps.empty;
            }
           
        }
    }

    public void board(){ 
        System.out.println();
        System.out.println(ps.padding+"Total move: "+ps.totalMove++);
        for (int l=0; l<size; l++){ 
            if (l%2 == 1) {
                ps.value = ps.empty;
            } else {
                ps.value = ps.wall;
            }
            for (int w=0; w<size; w++){
                defaultBoard(l);
                if (w ==0){
                    gameBoard[l][w] = String.valueOf(ps.padding+l) ;              
                }else if (l == 0){
                    gameBoard[l][w] = String.valueOf(w+ " ");
                }
                else {
                    gameBoard[l][w] = ps.value;
                }
                System.out.print(gameBoard[l][w]  +" ");
            }
            System.out.println();
        }
        System.out.println();
    }
    public static void main(String[] args) {
        
        GameBoard gb = new GameBoard();
        boolean err = gb.boardSize(3);{
            if(err){
                gb.board();
            }
        }
    }
}
