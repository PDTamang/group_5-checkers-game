package CheckerGame;

class Node {
    String data;
    Node next;
    Node(String data){
        this.data = data;
        next = null;
    }
}
public class LostPsStack {
    Node first;
    public LostPsStack(){
        first = null;
    }

    public void push(String element){
        Node newNode = new Node(element);
        newNode.next = first;
        first = newNode;
    }

    public boolean isEmpty(){
        return first == null;
    }

    public void traverse(){
        Node temp = first;
        while (temp != null){
            System.out.print(temp.data+" ");
            temp = temp.next;
        }
        System.out.println();
    }

    public String pop() {
        String value = first.data;
        first = first.next;
        return value;
    }

    String peek(){
        return first.data;
    }

    Boolean search(String element){
        Node temp = first;
        while (temp != null){
            if (temp.data.equals(element)) {
                temp = null;
                return true;
            };
             temp = temp.next;

        }
        return false;
    }


}

class Demo{
public static void main(String[] args) {
        // Scanner scan = new Scanner(System.in);
        LostPsStack stack = new LostPsStack();

        try{
            Object res = stack.pop();
            System.out.println(res);
        } catch(Exception e) {
            System.out.println(e);
        }
        stack.push("to");
        stack.push("who");
        // stack.push(9);

        stack.push("why");
        stack.push("where");
        stack.push("hwo");
        stack.push("where");
        // stack.push(9);

        stack.traverse();
        System.out.println(stack.pop());
        stack.traverse();
        System.out.println(stack.peek());
    }
}